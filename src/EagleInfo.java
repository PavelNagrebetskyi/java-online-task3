public class EagleInfo {
    public static void main(String[] args) {
        Animal aEagle = new Eagle();
        aEagle.sleep();
        aEagle.eat();
        aEagle.move();
    }
}
