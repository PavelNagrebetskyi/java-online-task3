public class ParrotInfo {
    public static void main(String[] args) {
        Animal aParrot = new Parrot();
        aParrot.sleep();
        aParrot.eat();
        aParrot.move();
    }
}
